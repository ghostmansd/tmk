#
# SPDX-License-Identifier: BSD-2-Clause
#
# Copyright (c) 2017-2019 Dmitry Selyutin.
# All rights reserved.
#

NUL?=/dev/null

-include $(CONFIG)

tmk/arch.tmk?=$(tmk/)arch.tmk
tmk/commands.tmk?=$(tmk/)commands.tmk
tmk/option.tmk?=$(tmk/)option.tmk
tmk/paths.tmk?=$(tmk/)paths.tmk
tmk/system.tmk?=$(tmk/)system.tmk
tmk/targets.tmk?=$(tmk/)targets.tmk
tmk/variables.tmk?=$(tmk/)variables.tmk

include $(tmk/arch.tmk)
include $(tmk/commands.tmk)
include $(tmk/system.tmk)
include $(tmk/paths.tmk)
include $(tmk/targets.tmk)
include $(tmk/variables.tmk)

CONFIG:=$(NUL)

tmk/tmk.tmk:=$(NUL)
